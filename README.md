# Project info
Currency converter implemented in JavaFX. The exchange rate is charged using api. There are several basic currencies to choose from. The program can easily be extended with additional currencies / functionalities.

# Project insight
![App](/misc/app-screen.png)
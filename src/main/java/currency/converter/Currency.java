package currency.converter;

import java.util.Map;

public class Currency {

    private String base;
    private String date;
    private Map<String,Double> rates;

    public Currency() {

    }

    public Currency(String base, String date, Map<String, Double> rates) {
        this.base = base;
        this.date = date;
        this.rates = rates;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Map<String, Double> getRates() {
        return rates;
    }

    public void setRates(Map<String, Double> rates) {
        this.rates = rates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Currency currency = (Currency) o;

        if (base != null ? !base.equals(currency.base) : currency.base != null) return false;
        if (date != null ? !date.equals(currency.date) : currency.date != null) return false;
        return rates != null ? rates.equals(currency.rates) : currency.rates == null;

    }

    @Override
    public int hashCode() {
        int result = base != null ? base.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (rates != null ? rates.hashCode() : 0);
        return result;
    }
}

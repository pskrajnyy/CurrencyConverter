package currency.converter.dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.IOException;
import java.util.Optional;

public class DialogUtils {

    public static Optional<ButtonType> dialogCloseApplication() {
        Alert closeInformation = new Alert(Alert.AlertType.CONFIRMATION);
        closeInformation.setTitle("Wyjście");
        closeInformation.setHeaderText("Czy na pewno chcesz wyjść z aplikacji?");
        Optional<ButtonType> result = closeInformation.showAndWait();
        return result;
    }

    public static void dialogInformationAboutApplication() {
        Alert informationAlert = new Alert(Alert.AlertType.INFORMATION);
        informationAlert.setTitle("O aplikacji");
        informationAlert.setHeaderText("Konwerter Walut, wersja aplikacji 1.0");
        informationAlert.setContentText("Moja pierwsza aplikacja wykonana w JavieFX");
        informationAlert.showAndWait();
    }

    public static void updateInfo(String date) throws IOException {
        Alert updateAlert = new Alert(Alert.AlertType.INFORMATION);
        updateAlert.setTitle("Aktualizacja kursów");
        updateAlert.setHeaderText("Kursy walut zostały zaktualizowane dnia: " + date);
        updateAlert.showAndWait();
    }
}

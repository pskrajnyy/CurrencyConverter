package currency.converter.controllers;

import currency.converter.Currency;
import currency.converter.dialogs.DialogUtils;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Optional;

public class MainController {

    private static final String URL_ADRESS = "https://api.exchangeratesapi.io/latest?base=";

    @FXML
    private Label labelAmountFrom;

    @FXML
    private Label labelAmountTo;

    @FXML
    private Label labelToCurrency;

    @FXML
    private Label labelFromCurrency;

    @FXML
    private TextField textField;

    @FXML
    private SplitMenuButton splitMenuFrom;

    @FXML
    private SplitMenuButton splitMenuTo;

    @FXML
    public void initialize() {
       textField.setText("0.00");
    }

    @FXML
    public void setLightTheme() {
        Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA);
    }

    @FXML
    public void setDarkTheme() {
        Application.setUserAgentStylesheet(Application.STYLESHEET_CASPIAN);
    }

    @FXML
    public void closeApplication() {
        Optional<ButtonType> result = DialogUtils.dialogCloseApplication();
        if(result.orElse(ButtonType.OK) == ButtonType.OK){
            Platform.exit();
            System.exit(0);
        }
    }

    @FXML
    public void aboutApplication() {
        DialogUtils.dialogInformationAboutApplication();
    }

    public void update() throws IOException {
        DialogUtils.updateInfo(request().getDate());
    }

    public void setFromPln() {
        labelFromCurrency.setText("PLN");
        splitMenuFrom.setText("PLN");
    }

    public void setFromUsd() {
        labelFromCurrency.setText("USD");
        splitMenuFrom.setText("USD");
    }

    public void setFromEur() {
        labelFromCurrency.setText("EUR");
        splitMenuFrom.setText("EUR");
    }

    public void setFromGbp() {
        labelFromCurrency.setText("GBP");
        splitMenuFrom.setText("GBP");
    }

    public void setToGbp() {
        splitMenuTo.setText("GBP");
        labelToCurrency.setText("GBP");
    }

    public void setToPln() {
        splitMenuTo.setText("PLN");
        labelToCurrency.setText("PLN");
    }

    public void setToUsd() {
        splitMenuTo.setText("USD");
        labelToCurrency.setText("USD");
    }

    public void setToEur() {
        splitMenuTo.setText("EUR");
        labelToCurrency.setText("EUR");
    }

    public void exchangeCurrency()  {
        if(labelFromCurrency.getText().equals(labelToCurrency.getText())) {
            labelAmountFrom.setText(textField.getText());
            labelAmountTo.setText(labelAmountFrom.getText());
        } else {
            try{
                Double fromValue = Double.parseDouble(textField.getText().replaceAll(",","\\."));
                Double toValue = fromValue * request().getRates().get(labelToCurrency.getText());
                labelAmountFrom.setText(textField.getText().replaceAll("\\.",","));
                labelAmountTo.setText(editDouble(toValue));
            } catch(NumberFormatException e) {
                textField.setText("");
            }
        }
    }

    private Currency request(){
        Client client = ClientBuilder.newClient();
        return client.target(URL_ADRESS + labelFromCurrency.getText() + "&symbols=" + labelToCurrency.getText())
                .request(MediaType.APPLICATION_JSON)
                .get(Currency.class);
    }

    private String editDouble(Double value) {
        DecimalFormat formatDouble = new DecimalFormat("#0.00");
        return formatDouble.format(value);
    }
}
